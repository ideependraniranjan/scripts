#!/bin/bash
# Md. Obydullah is here!
# Full backup script

# export database
echo "Exporting kamla hospital database..."
sudo mysqldump -u kamla_hsptl -p6BYkOYagmnSNbxyA kamla_hsptl > /usr/share/backups/dbs/kamla-backup.sql
echo "kamla hospital database exported."
echo ""

echo "Exporting hariom crusher mongo dump..."
sudo mongodump -d amit_crusher -o /usr/share/backups/dbs/hariomcrusher
echo "hariom db exported"
echo ""

echo "Exporting dilip crusher mongo dump..."
sudo mongodump -d dilip_crusher -o /usr/share/backups/dbs/dilipcrusher
echo "dilip db exported"
echo ""

# compress the directory
echo "Compressing directory..."
tar -zcvf "db-backup-$(date '+%d-%m-%Y-%H-%M').tgz" /usr/share/backups/dbs/
echo "Directory compressed."
echo ""

# upload to google drive
echo "Uploading to Google Drive..."
/usr/sbin/drive upload --file "db-backup-$(date '+%d-%m-%Y-%H-%M').tgz" -p 1EGyfFsEE4v07_QiORONNsCGd5ktKZIVj
echo "Uploaded to Google Drive."
echo ""

# remove the database and compressed file
echo "Clearing temporary files..."
sudo rm -rf /usr/share/backups/dbs/hariomcrusher
sudo rm	-rf /usr/share/backups/dbs/dilipcrusher
sudo rm /usr/share/backups/dbs/kamla-backup.sql
sudo rm "db-backup-$(date '+%d-%m-%Y-%H-%M').tgz"
echo "Cleared temporary files."
echo ""