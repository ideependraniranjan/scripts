# kill all monngodb processes
echo 'killing all mongo processes'
killall mongod

# restart php-fpm
echo 'Restarting PHP service'
systemctl restart php-fpm

# restart mariadb
echo 'Restarting mariadb'
systemctl restart mariadb

# restart mongodb
echo 'Restarting mongo service'
mongod --config /etc/mongod.conf

# restart pm2
echo 'Restarting pm2 services'
/root/.nvm/versions/node/v12.20.2/bin/pm2 restart all

# restart nginx
echo 'Restarting Nginx engine'
systemctl restart nginx