# install google drive
wget -O drive https://drive.google.com/uc?id=0B3X9GlR6EmbnMHBMVWtKaEZXdDg
mv drive /usr/sbin/drive
chmod 755 /usr/sbin/drive

# certbot renew 
certbot certonly --force-renew -d *.thetechassembles.com --dns-cloudflare-credentials /root/.secrets/cloudflare.ini
certbot renew --dry-run
# certbot cron script
certbot renew --quiet --no-self-upgrade

# port enable info
netstat -ntlp
lsof -i -P |grep http
iptables-save | grep 27017
